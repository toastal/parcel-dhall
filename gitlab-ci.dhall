-- let GitLab =
--      https://raw.githubusercontent.com/bgamari/dhall-gitlab-ci/75fedc390d035a715e1856d94227ddacc9dc5c71/GitLab/package.dhall sha256:2e198f2e65bf080d67fbec13af1c0fa5896a727c63a818e23b81860a6a48e9a6
-- Would be cool to use, but couldn’t find the `only`

let Stage = < publish >

let Package
    : Type
    = { name : Text, directory : Text }

let Job
    : Type
    = { stage : Stage
      , script : List Text
      , only : { refs : List Text, changes : List Text }
      }

let publisher
    : Package → Job
    = λ(pkg : Package) →
        { stage = Stage.publish
        , script =
          [ "cd ${pkg.directory}"
          , "echo 'Building ${pkg.name}'"
          , "NPM_TOKEN='\${NPM_AUTH_TOKEN}' npm publish"
          ]
        , only =
          { refs = [ "trunk", "merge_requests" ]
          , changes = [ "${pkg.directory}/**/*" ]
          }
        }

let pkgPrefix
    : Text
    = "@toastal/parcel-dhall"

in  { image = "node:current-alpine"
    , stages = [ Stage.publish ]
    , publish-utils =
        publisher { name = "${pkgPrefix}-utils", directory = "utils" }
    , publish-transformer-json =
        publisher
          { name = "${pkgPrefix}-transformer-json"
          , directory = "transformers/json"
          }
    , publish-transformer-webmanifest =
        publisher
          { name = "${pkgPrefix}-transformer-webmanifest"
          , directory = "transformers/webmanifest"
          }
    }
