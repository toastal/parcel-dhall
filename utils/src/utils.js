// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2020 toastal parcel-utils-dhall
"use strict"

const { spawn } = require("child_process")
const commandExists = require("command-exists")

module.exports = {
  tryCommandExists(cmd) {
    return new Promise((resolve, reject) => {
      try {
        return commandExists(cmd)
      } catch (e) {
        throw new Error("`" + cmd + "` isn’t installed on the $PATH.")
      }
    })
  },

  renderJSON(config, contentStr) {
    // TODO: use config
    const args = [
      "--compact",
      "--omit-empty",
    ]

    const dtj = spawn("dhall-to-json", args)

    const errors = []
    const result = []

    /* eslint-disable fp/no-mutating-methods */
    dtj.stderr.on("data", (err) => { errors.push(err) })
    dtj.stdout.on("data", (res) => { result.push(res) })
    /* eslint-enable */

    dtj.stdin.write(contentStr)
    dtj.stdin.end()

    return new Promise((resolve, reject) => {
      dtj.on("close", (code) => {
        if (code !== 0) {
          const errorsStr = Buffer.concat(errors).toString("utf-8")
          return reject(new Error(errorsStr))
        } else {
          const resultBuf = Buffer.concat(result)
          return resolve(resultBuf)
        }
      })
    })
  },
}
