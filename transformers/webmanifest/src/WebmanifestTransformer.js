// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2020 toastal parcel-tranformer-dhall-webmanifest
//
// A lot of this could be removed in favor of this PR
// https://github.com/parcel-bundler/parcel/pull/4313
//
// “Borrowing” code from here in the meantime:
// https://github.com/parcel-bundler/parcel/blob/webmanifest/packages/transformers/webmanifest/src/WebManifestTransformer.js
"use strict"

const { Transformer } = require("@parcel/plugin")
const utils = require("@toastal/parcel-utils-dhall")
const { getJSONSourceLocation } = require("@parcel/diagnostic")
const jsonSourceMap = require("json-source-map")

// this is a mutabale asset
function collectDependencies(asset, source, data, pointers) {
  return [ "icons", "screenshots" ].forEach((resourceType) => {
    ;(data[resourceType] || []).forEach((resource, i) => {
      resource.src = asset.addURLDependency(resource.src, {
        loc: {
          filePath: asset.filePath,
          ...getJSONSourceLocation(pointers[`/${resourceType}/${i}/src`], "value"),
        },
      })
    })
  })
}

module.exports = new Transformer({
  async transform({ asset, config, options }) {
    utils.tryCommandExists("dhall-to-json")

    const contents =
      await asset.getCode()
        .then((c) => utils.renderJSON(config, c).toString("utf-8"))

    const { data, pointers } = jsonSourceMap.parse(contents)
    collectDependencies(asset, contents, data, pointers)

    asset.type = "webmanifest"
    asset.setCode(JSON.stringify(data))
    return [ asset ]
  },
})
