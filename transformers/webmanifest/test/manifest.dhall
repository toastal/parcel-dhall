let WebAppManifest =
      https://gitlab.com/toastal/dhall-webmanifest/raw/trunk/Webmanifest/WebAppManifest.dhall

in  WebAppManifest::{ name = Some "Test", lang = Some "en-US" }
