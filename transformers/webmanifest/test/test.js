/* eslint-env mocha */
"use strict"
// this doesn't work

const path = require("path")
// where does this come from?
const { bundle, assertBundles } = require("@parcel/test-utils")

describe("dhall-webmanifest", function() {
  it("should bundle a `.webmanifest`", async function() {
    const b = await bundle(
      path.join(__dirname, "testpage.html"),
    )

    assertBundles(b, {
      name: "testpage.html",
      assets: [ "testpage.html" ],
      childBundles: [
        {
          type: "webmanifest",
          assets: [ "manifest.webmanifest" ],
          childBundles: [],
        },
      ],
    })
  })
})
