# parcel-transformer-dhall-webmanifest

Configure your progressive web app (PWA) with a Web App Manifest using and the Dhall configuration language. You can include [`dhall-webmanifest`](https://gitlab.com/toastal/dhall-webmanifest) for types.


## Usage

Add to your `.parcelrc` file.

```json
{
  "extends": "@parcel/config-default",

  "transformers": {
    "manifest.dhall": [
      "@toastal/parcel-transformer-dhall-webmanifest",
    ]
  }
}
```

Create a file like `./manifest.dhall`

```dhall
-- Pick a tag/commit hash! Freeze it too!
let WebAppManifest =
      https://gitlab.com/toastal/dhall-webmanifest/raw/trunk/Webmanifest/WebAppManifest.dhall sha:…

in    WebAppManifest::{
      , name = Some "Hello, world!"
      , lang = Some "en-US"
      , dir = Some WebAppManifest.TextDirection.Type.ltr
      , display = Some WebAppManifest.DisplayMode.Type.browser
      , orientation = Some WebAppManifest.OrientationLock.Type.natural
      , theme_color = Some "#f2008a"
      , background_color = Some "#f8f8f6"
      , categories = Some [ "blog", "programming" ]
      , icons = Some
        [ { src = "icon.avif"
          , sizes = Some "64x64"
          , type = Some "image/avif"
          }
        , { src = "favicon.ico"
          , sizes = Some "16x16 32x32"
          , type = None Text
          }
        , WebAppManifest.ImageResource::{ src = "icon.svg" }
        ]
      }
    : WebAppManifest.Type
```
(sorry, no syntax Dhall highlighting on GitLab or NPM; [`rouge` issue #1054](https://github.com/rouge-ruby/rouge/issues/1054), [`highlight.js` language not supported](https://github.com/highlightjs/highlight.js/blob/master/SUPPORTED_LANGUAGES.md))

Slap it in your `index.html` file

```html
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta name="viewport" content="width=device-width,minimum-scale=1,user-scalable=no">
    <link rel="manifest" href="./manifest.dhall">
  </head>
  <body>Hello, world!</body>
</html>
```

- - -


## License

This project is licensed under the BSD 3-Clause “New” or “Revised” License - see the [LICENSE](./LICENSE) file for details.


## Funding

If you want to make a small contribution to the maintanence of this project

- [Liberapay](https://liberapay.com/toastal/)
