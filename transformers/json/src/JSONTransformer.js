// SPDX-License-Identifier: BSD-3-Clause
// Copyright © 2020 toastal parcel-transformer-dhall-json
"use strict"

const { Transformer } = require("@parcel/plugin")
const utils = require("@toastal/parcel-utils-dhall")

module.exports = new Transformer({
  async transform({ asset, config, options }) {
    utils.tryCommandExists("dhall-to-json")

    const json =
      await asset.getCode().then((c) => utils.renderJSON(config, c))

    asset.type = "json"
    asset.setCode(json)

    return [ asset ]
  },
})
