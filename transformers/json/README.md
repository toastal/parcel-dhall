# parcel-transformer-dhall-json

Use the Dhall configuration language for JSON.


## Usage

Add to your `.parcelrc` file.

```json
{
  "extends": "@parcel/config-default",

  "transformers": {
    "manifest.dhall": [
      "@toastal/parcel-transformer-dhall-json",
    ]
  }
}
```

- - -


## License

This project is licensed under the BSD 3-Clause “New” or “Revised” License - see the [LICENSE](./LICENSE) file for details.


## Funding

If you want to make a small contribution to the maintanence of this project

- [Liberapay](https://liberapay.com/toastal/)
